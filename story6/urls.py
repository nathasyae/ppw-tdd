from django.urls import include, path
from django.conf.urls import url
from .views import *
from django.contrib.auth import views	

urlpatterns = [
	path('home',index,name='home'),
	path('',index, name='home'),
	path('profile',profile, name='profile'),
	path('library',library,name='library'),
	path('thebooks',thebooks,name='thebooks'),
	# path('registration',registration,name='registration'),
	#path('form',form,name='form'),
	path('subs',subs,name='subs'),
	path('cekEmail',cekEmail,name='cekEmail'),
	path('addtodb',addtodb,name='addtodb'),
	path('oauth',include('social_django.urls',namespace='social')),
	path('login',login,name='login'),
	# 	path('login', auth_views.login, name='login'),

]
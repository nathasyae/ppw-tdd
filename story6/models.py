from django.db import models

# Create your models here.
class Status(models.Model):
    status = models.CharField(max_length=300)
    created_time = models.DateTimeField(auto_now_add=True)
    # ngecek = models.CharField(max_length=50)

class Account(models.Model):
    name = models.CharField(max_length=75)
    email = models.EmailField(max_length=75)
    password = models.CharField(max_length=20)
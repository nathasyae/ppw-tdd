from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .views import profile
from .models import Status
from .forms import StatusForm

from selenium import webdriver
import time
import unittest
'''

class FunctionalTest(unittest.TestCase):
	def setUp(self):
		self.browser = webdriver.Chrome()
		self.browser.get('http://localhost:8000/story6/')
		time.sleep(1)
		super(FunctionalTest,self).setUp()

	def test_can_input_status(self):
    # find the element
		status = self.browser.find_element_by_id('id_status')
		submit = self.browser.find_element_by_id('submit')

        # fill in the blankkKKKkk
		status.send_keys('Coba Coba')

        # submit
		submit.submit()

		# cek udah ada apa blm
		table = self.browser.find_element_by_tag_name('th').text
		self.assertIn('Coba Coba', self.browser.page_source)
	
	def tearDown(self):
		self.browser.implicitly_wait(5)
		self.browser.quit()
		super(FunctionalTest,self).tearDown()


# Create your tests here.
class Story6UnitTest(TestCase):    
	def test_story6_url_is_exist(self):
		response = Client().get('/story6/')
		self.assertEqual(response.status_code,200)
		
	def test_function_index_caller_exist(self):
		found = resolve('/story6/')
		self.assertEqual(found.func, index)

	def test_home_containt(self):
		response = self.client.get('/story6/')
		html_response = response.content.decode('utf8')
		self.assertIn('Hello, apa kabar?', html_response)

	def test_new_status(self):
		testText = 'test'
		new_act = Status.objects.create(status= testText)
		counting_all_available_status =  Status.objects.all().count();
		self.assertEqual(counting_all_available_status, 1)

	def test_form_status_rendered(self):
		response = Client().get('/story6/')
		html_response = response.content.decode('utf8')
		self.assertIn('Status', html_response)

	def test_post_success_and_render(self):
		testText = 'test'
		response_post = Client().post('/story6/', {'status': testText})
		self.assertEqual(response_post.status_code, 302)		
		response= Client().get('/story6/')
		html_response = response.content.decode('utf8')
		self.assertIn(testText, html_response)

	def test_TextField_300char(self):
		testText = "a" * 301
		response = Client().post("/story6/", {"status": testText})
		self.assertEqual(Status.objects.filter(status = testText).count(), 1)


	#LETS DO THE CHALLENGE YUHU

	def test_profile_url_is_exist(self):
		response = Client().get('/story6/profile')
		self.assertEqual(response.status_code,200)

	def test_profile_containt(self):
		response = self.client.get('/story6/profile')
		html_response = response.content.decode('utf8')
		self.assertIn('Nathasya', html_response)

	def test_function_profile_caller_exist(self):
		found = resolve('/story6/profile')
		self.assertEqual(found.func, profile)
	
	# STORY 8 TEST
	def test_profile_containt(self):
		response = self.client.get('/story6/profile')
		html_response = response.content.decode('utf8')
		self.assertIn('yang saya lakukan saat ini?', html_response)
		

	# STORY 9 TEST
	def test_function_library_caller_exist(self):
		found = resolve('/story6/library')
		self.assertEqual(found.func, index)

	def test_library_containt(self):
		response = self.client.get('/story6/library')
		html_response = response.content.decode('utf8')
		self.assertIn('HARUS BGTTT BACA BUKU INI WOI', html_response)


		'''
from django.shortcuts import render, reverse
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.core.validators import validate_email
from .models import *
from .forms import *
import requests	


# Create your views here.
response={}

# STORY 11
def login(request):
	return render(request, 'login.html')

# STORY 8

def home(request):
	return render(request, 'home.html')


def index(request):
	data = Status.objects.all()
	if request.method == 'POST':
		form =  StatusForm(request.POST)
		if form.is_valid():
			new_status = Status()
			new_status.status = form.cleaned_data['status']
			new_status.save()
			return HttpResponseRedirect('/story6/')
	else:
		form = StatusForm()
	return render(request, 'home.html', {'form':form, 'data':data})

def profile(request):
	return render(request, 'profile.html')

# STORY 9
def library(request):
	return render(request, 'library.html', response)

def thebooks(request,searchkey="quilting"):
	link = 'https://www.googleapis.com/books/v1/volumes?q=' + searchkey
	listbuku = requests.get(link).json()
	return JsonResponse(listbuku)

# STORY 10		
def subs(request):
	response = {'RegistForm':RegistForm}
	return render (request, 'registration.html',response)

def cekEmail(request):
    try:
        print(request.POST['email'])
        validate_email(request.POST['email'])
    except:
        return JsonResponse({
            'message':'benerin formatnya emailnya coba',
            'status':'fail'
        })

    exist = Account.objects.filter(email=request.POST['email'])

    if exist:
        return JsonResponse({
			'message':'email dah ada',
            'status':'fail'
        })
    
    return JsonResponse({
        'message':'email ok',
        'status':'success'
    })

def addtodb(request):
	if (request.method == "POST"):
			subscriber =Account(
				email = request.POST['email'],
				name = request.POST['name'],
				password = request.POST['password']   
			)
			subscriber.save()
			return JsonResponse({
				'message':'km dah subskraip'
			})

	else:
		return JsonResponse({
				'message':"wadaw saya cari method post ne"
			})
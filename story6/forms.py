from django import forms
from .models import *
from django.utils import timezone
from datetime import *

class StatusForm(forms.Form):
    status = forms.CharField(widget = forms.Textarea(
        attrs= {
            'required' : True,
            'placeholder' : 'tell anything',
        }
    ))

class RegistForm(forms.Form):
    name = forms.CharField(label = "name", max_length = 75, widget = forms.TextInput(
        attrs = {
            'class': 'form-attr',
            'placeholder': 'your name',
            'required': True,
        }
    ))

    email = forms.EmailField(label = "e-mail", widget = forms.EmailInput(
        attrs = {
            'class': 'form-attr',
            'required': True,
            'placeholder': 'name@email.com',
        }
    ))

    password = forms.CharField(label = "password", widget = forms.PasswordInput(
        attrs = {
            'class': 'form-attr',
            'required': True,
            'placeholder': 'password',
        }
    ))

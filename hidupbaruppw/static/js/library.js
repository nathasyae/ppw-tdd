$(document).ready(function(e) {   

$(function(){
  $.ajax({
      type : "GET",
      url: 'data',
      dataType: 'json',
      success: function(data){
        var result = ''
        var temp = data['items']
        for (var i = 0; i<temp.length; i++) {
          item = temp[i]
          var img = item['volumeInfo']['imageLinks']['smallThumbnail']
          var title = item['volumeInfo']['title']
          var author = item['volumeInfo']['authors']
          var desc = item['volumeInfo']['description']

          result += "<tr><td><img src =" + img + "></td>"
          result += "<td>" + title + "</td>"
          result += "<td>" + author + "</td>"
          result += "<td>" + desc + "</td>"
          result += "<td><button id = 'star'><i class=\"fa fa-star\"></i></button><td>"
        }
        $(".isitable").html(result)
      }
    });
  });
 

  var fav = 0;
  $(document).on('click', '#star', function(){
    if ($(this).hasClass('clicked')) {
      fav--;
      $(this).removeClass('clicked');
      $(this).css("color", "grey");
    }
    else{
      fav++;
      $(this).addClass('clicked');
      $(this).css("color","#fe6287");
    }
    $("#counter").html(fav)
  });
});
$(document).ready(function(){
  $("#search").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $(".isitable").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});

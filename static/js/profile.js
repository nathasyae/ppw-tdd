
// //$( function() {
//     $( "#progressbar" ).progressbar({
//       value: 37
//     });
//   } );

$(document).ready(function(e) {   

    $('#btnlight').click(function (e) {   
        $('.body-dark').removeClass('body-dark').addClass('body-light')
        $('#navbarq').removeClass('navbar navbar-expand-lg navbar-dark bg-dark').addClass('navbar navbar-expand-lg navbar-light bg-light')
    });

    $('#btndark').click(function (e) {   
        $('.body-light').removeClass('body-light').addClass('body-dark') 
        $('#navbarq').removeClass('navbar navbar-expand-lg navbar-light bg-light').addClass('navbar navbar-expand-lg navbar-dark bg-dark');
    });

    $(".openpanel").on("click", function() {
        $("#panel3").collapse('show');
     });
     $(".closepanel").on("click", function() {
        $("#panel3").collapse('hide');
     });
     $('#accordion').on('show.bs.collapse', function () {
        $('#accordion .in').collapse('hide');
     });
    });

    $.ajax({
        url: "data",
        datatype: 'json',
        success: function(result){
            var books_object = jQuery.parseJSON(result);
            renderHTML(books_object);
        }
    });
     
    
    function renderHTML(data){
        var books_list = data.items;
              for (var i = 0; i < books_list.length; i++){
                var title = books_list[i].volumeInfo.title;
                var author = books_list[i].volumeInfo.authors[0];
                var about = books_list[i].volumeInfo.description;
                var temp = title.replace(/'/g, "\\'");
                var table = 
                '<tr class="table-books">'+
                '<td class = "img">' + "<img src='"+data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>"+
                '<td class="title">'+title+'</td>'+
                '<td class= "author">'+author+'</td>'+
                '<td class= "about">'+about+'</td>'+
                '<td class= "favourite">' + '<button id = "star" ><i class = "fas fa-star"</i></button>' + '</td>';
                $('#isitable').append(table);
              }
      }

var emailvalid = false;

$(document).ready(function() {
    $('input').focusout(function() {
        cek();
    });
    
    $('#emailnye').keyup(function() {
        cekEmail();
    });

    $('#submit').click(function () {
        data = {
            'email' : $('#emailnye').val(),
            'name' : $('#namanye').val(),
            'password' : $('#passnye').val(),
            "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
        }
        $.ajax({
            type : 'POST',
            url : '/story6/addtodb',
            data : data,
            dataType : 'json',
            success : function(data) {
                alert(data['message']);
                document.getElementById('email').value = '';
                document.getElementById('name').value = '';
                document.getElementById('password').value = '';
                
                $('#catatankaki').html('');
                cek();
            }
        })
    });
})


function cekEmail() {
    data = {
        'email':$('#emailnye').val(),
        "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
    }
    $.ajax({
        type: "POST",
        url: '/story6/cekEmail',
        data: data,
        dataType: 'json',
        success: function(data) {
            $('#catatankaki').html('');
            if (data['status'] === 'fail') {
                emailvalid = false;
                $('#submit').prop('disabled', true);
                $('#catatankaki').append('<small style="color:red">' + data["message"] + '</small>');
            } else {
                emailvalid = true;
                cek();
                $('#catatankaki').append('<small style="color:green">' + data["message"] + '</small>');
            }
        }
    });
}

function cek() {
    if (emailvalid && $('#namanye').val() !== '' && $('#passnye').val() !== ''){
        $('#submit').prop('disabled', false);
    } else {
        $('#submit').prop('disabled', true);
    }
}